$(function () {
	$("#student").click(function (e) {
		e.preventDefault();
		if ( $(this).val() == 0 ) {
			return false;
		}
		var student = $('#student').val();
		if ( student == 0 ) {
			$("#response").text("Выберите ученика!").show().delay(1500).fadeOut(800);
			return false;
		}
		$.ajax({
			type    : "POST",
			url     : "/progress/select",
			data    : {"student_id" : student},
			cache   : false,
			success : function (response) {
				if ( response != 0 ) {
					console.log(response[0]);
					 $('#group_store_id').val(response[0]);
					$("#respon").fadeIn(500);
				} else {
					$("#response").text("Извините, ошибка.").show().delay(1500).fadeOut(800);
					return false;
				}
			}
		});
	});
});


