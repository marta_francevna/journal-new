$(function () {

    $("#course").click(function (e) {
        e.preventDefault();
        if ($(this).val() == 0) return false;
        var id_gs = $(this).val();
        var resp = document.getElementById('resp1');
        var form = document.getElementById('form');
        var inp = $('#inp');
	    var course_number = document.getElementById('course_number');
	    var course = document.getElementById('course');
	    course_number.value =course.options[course.selectedIndex].text;
	    $.ajax({
            type: "POST",
            url: "/semester-student",
            data: {"id_gs": id_gs},
            cache: false,
            success: function (response) {
                removeChildren(resp);
                $('.table').fadeOut(300);
                $('.b-text').fadeOut(300);
                $('.sorry').fadeOut(300);
                var p = document.createElement("p");
                resp.appendChild(p);
                if(response[0].length>0) {

	                var label         = document.createElement("label");
                    if(response[1]==1){
                        label.textContent = 'Выберите семестр:';
                    }else {
                        label.textContent = 'Выберите полугодие:';
                    }
	                p.appendChild(label);

	                var select  = document.createElement("select");
	                select.name = 'semester';
	                p.appendChild(select);

	                for ( var i = 0; i < response[0].length; i++ ) {
		                var option  = document.createElement("option");
		                option.text = response[0][i].semester;
		                select.appendChild(option);
		                var div   = document.createElement("div");
		                div.className = "b-button button-custom";
		                p.appendChild(div);

	                }
                    var inp   = document.createElement("input");
                    inp.type  = "submit";
                    inp.value = "Показать";
                    inp.className = "button";
                    inp.style.marginRight = '0';
                    div.appendChild(inp);
                }else {
	                var pre   = document.createElement("p");
	                pre.textContent = 'Извините, нет успеваемости за этот курс';
	                p.appendChild(pre);
                }

            }
        });
    });
});

function removeChildren(node) {
    var children = node.childNodes;

    for (var i = 0; i < children.length; i++) {
        var child = children[i];
        node.removeChild(child)
    }
}