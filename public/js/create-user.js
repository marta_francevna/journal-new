$(function () {
	$("#b1").click(function (e) {
		e.preventDefault();
		if ( $(this).val() == 0 ) {
			return false;
		}
		var group = $('#group').val();
		var year  = $('#years').val();
		if ( group == 0 ) {
			$("#response").text("Выбери группу!").show().delay(1500).fadeOut(800);
			return false;
		}
		$.ajax({
			type    : "POST",
			url     : "/students/select",
			data    : {"group" : group},
			cache   : false,
			success : function (response) {
				if ( response != 0 ) {
					$('#group_id').val(response[0]);
					$('#year').val(year);
					$("#resp1").fadeOut(500);
					$("#resp2").fadeIn(500);
				} else {
					$("#response").text("Извините, нет такой группы ").show().delay(1500).fadeOut(800);
					return false;
				}
			}
		});
	});
});

