<?php
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Text;
use Phalcon\Validation\Validator\Date;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Regex as RegexValidator;
use Phalcon\Validation\Validator\StringLength;

class GroupsForm extends \Phalcon\Forms\Form
{
	public function initialize($entity = null, $options = null)
	{

		$name = new Text("name");

		$name->addValidator(
			new StringLength([
				'max'            => 15,
				'min'            => 2,
				'messageMaximum' => 'Название не может содержать больше 15 символов.',
				'messageMinimum' => 'Название не может содержать меньше 5 символов.',
			]));

		$name->addValidator(
			new RegexValidator([
				'pattern' => '/^[а-яА-Яa-zA-Z0-9_-]+$/u',
				'message' => 'Название может содержать только русские буквы, цифры и тире.',
			]));

		$this->add($name);


		$sort = new \Phalcon\Forms\Element\Select("sort",
			[
				0 => "ПТО",
				1 => "ССО",
			]
		);
		$this->add($sort);

		$course = new Text("course");

		$course->addValidator(
			new StringLength([
				'max'            => 2,
				'min'            => 1,
				'messageMaximum' => 'Название не может содержать больше 2 символов.',
				'messageMinimum' => 'Название не может содержать меньше 1 символа.',
			]));

		$course->addValidator(
			new RegexValidator([
				'pattern' => '/^[0-9]+$/u',
				'message' => 'Название может содержать только  цифры.',
			]));


		$this->add($course);

	}
}