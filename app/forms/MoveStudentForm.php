<?php
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Text;
use Phalcon\Validation\Validator\Date;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Regex as RegexValidator;
use Phalcon\Validation\Validator\StringLength;

class MoveStudentForm extends \Phalcon\Forms\Form
{
	public function initialize($entity = null, $options = null)
	{

		$user_id = new \Phalcon\Forms\Element\Hidden('user_id');

		$this->add($user_id);

		$group_id = new \Phalcon\Forms\Element\Select("group_id", Group::find(['order' => 'name']), [
			"using" => [
				"id",
				"name",
			],
		]);

		$this->add($group_id);

		$today = date("Y");

		$year = new \Phalcon\Forms\Element\Numeric("year", [
			"min"   => 1900,
			"max"   => 2099,
			"step"  => 1,
			"value" => $today,
		]);

		$this->add($year);

		$status = new \Phalcon\Forms\Element\Hidden("status", [
			'value' => 0,
		]);

		$this->add($status);
	}
}