<?php
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Text;
use Phalcon\Validation\Validator\Date;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Regex as RegexValidator;
use Phalcon\Validation\Validator\StringLength;

class GroupStoreForm extends \Phalcon\Forms\Form
{
	public function initialize($entity = null, $options = null)
	{

		$user_id = new Text("user_id");

		$this->add($user_id);

		$group_id = new Text("group_id");

		$this->add($group_id);

		$year = new Text("year");

		$this->add($year);

		$status = new Text("status", [
			'value' => 0,
		]);

		$this->add($status);
	}
}