<?php

class Progress extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=10, nullable=false)
     */
    protected $id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=10, nullable=false)
     */
    protected $subject_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=10, nullable=false)
     */
    protected $group_store_id;

    /**
     *
     * @var double
     * @Column(type="double", nullable=false)
     */
    protected $grade;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $omission;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $semester;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field subject_id
     *
     * @param integer $subject_id
     * @return $this
     */
    public function setSubjectId($subject_id)
    {
        $this->subject_id = $subject_id;

        return $this;
    }

    /**
     * Method to set the value of field group_store_id
     *
     * @param integer $group_store_id
     * @return $this
     */
    public function setGroupStoreId($group_store_id)
    {
        $this->group_store_id = $group_store_id;

        return $this;
    }

    /**
     * Method to set the value of field grade
     *
     * @param double $grade
     * @return $this
     */
    public function setGrade($grade)
    {
        $this->grade = $grade;

        return $this;
    }

    /**
     * Method to set the value of field omission
     *
     * @param integer $omission
     * @return $this
     */
    public function setOmission($omission)
    {
        $this->omission = $omission;

        return $this;
    }

    /**
     * Method to set the value of field semester
     *
     * @param integer $semester
     * @return $this
     */
    public function setSemester($semester)
    {
        $this->semester = $semester;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field subject_id
     *
     * @return integer
     */
    public function getSubjectId()
    {
        return $this->subject_id;
    }

    /**
     * Returns the value of field group_store_id
     *
     * @return integer
     */
    public function getGroupStoreId()
    {
        return $this->group_store_id;
    }

    /**
     * Returns the value of field grade
     *
     * @return double
     */
    public function getGrade()
    {
        return $this->grade;
    }

    /**
     * Returns the value of field omission
     *
     * @return integer
     */
    public function getOmission()
    {
        return $this->omission;
    }

    /**
     * Returns the value of field semester
     *
     * @return integer
     */
    public function getSemester()
    {
        return $this->semester;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("journal");
        $this->belongsTo('group_store_id', '\GroupStore', 'id', ['alias' => 'GroupStore']);
        $this->belongsTo('subject_id', '\Subject', 'id', ['alias' => 'Subject']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'progress';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Progress[]|Progress
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Progress
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
