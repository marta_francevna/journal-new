<?php

use Phalcon\Mvc\View;
use Phalcon\Paginator\Adapter\Model as PaginatorModel;
use Phalcon\Paginator\Adapter\NativeArray as PaginatorArray;

class StudentsController extends ControllerBase
{

    public function indexAction()
    {

        $group = Group::find([
            "columns" => "name",
            'group' => 'name',
        ]);
        $this->view->setVars([
            'group' => $group,
        ]);

    }

    public function groupAction()
    {
        $group = $this->request->getPost('group');
        $year = $this->request->getPost('year');
        $group_id = Group::findFirst([
           'conditions'=>'name = :name:',
            'bind'=>[
                'name'=>$group
            ]
        ]);
        if($group_id){
            $students = $this->modelsManager->createBuilder()
                ->columns(['u.name', 'u.number', 'u.status', 'u.id', 'gs.group_id', 'gs.year'])
                ->addFrom(Users::class, 'u')
                ->innerJoin(GroupStore::class, 'u.id = gs.user_id', 'gs')
                ->where("u.role = :role: and gs.group_id = :group: and gs.year = :year:", [
                    'role' => 0,
                    'group' => $group_id->getId(),
                    'year' => $year
                ])
                ->getQuery()
                ->execute()
                ->toArray();

            $this->view->setVars([
                'students' => $students,
                'group' => $group
            ]);

        }
    }

    public function addAction()
    {
        $groups = Group::find([
            'order' => 'name ASC',
        ]);
        $form = new UserForm();
        $formGS = new GroupStoreForm();
        $student = new Users();
        $group_store = new GroupStore();

        $this->view->setVars([
            'form' => $form,
            'formGS' => $formGS,
            'student' => $student,
            'group' => $groups,
        ]);

        if (!$this->request->isPost()) {
            return $this->view;
        }

        if ($form->isValid($this->request->getPost(), $student)) {

            if ($student->create()) {
                $formGS->bind($this->request->getPost(), $group_store);
                $group_store->setUserId($student->getId());
                $group_store->create();

                return $this->response->redirect('/students', true);
            }
        }
    }

    public function editAction()
    {
        $id = $this->dispatcher->getParam('id');
        $group_id = $this->dispatcher->getParam('group_id');

        $student = Users::findFirst([
            'conditions' => 'id = :id: ',
            'bind' => [
                'id' => $id,
            ]]);

        $gs = GroupStore::findFirst([
            'conditions' => 'user_id = :user_id: and group_id = :group_id:',
            'bind' => [
                'user_id' => $id,
                'group_id' => $group_id,
            ],
        ]);
        $form = new UserForm($student);
        $formGS = new GroupStoreEditForm($gs);
        $this->view->setVars([
            'form' => $form,
            'student' => $student,
            'formGS' => $formGS,
            'group_id' => $group_id,
        ]);
        if (!$this->request->isPost()) {
            return $this->view;
        }
        if ($form->isValid($this->request->getPost(), $student)) {

            if ($student->save()) {
                $formGS->bind($this->request->getPost(), $gs);
                if ($gs->save()) {
                    return $this->response->redirect('/students', true);
                };
            }
        }

    }

    public function delAction()
    {
        $id = $this->dispatcher->getParam('id');
        $student = Users::findFirst([
            'conditions' => 'id = :id: ',
            'bind' => [
                'id' => $id,
            ]]);
        $student->delete();

        return $this->response->redirect('/students', true);
    }

    public function selectAction()
    {

        $group = $this->request->getPost('group');

        $group_id = Group::findFirst([
            'conditions' => 'name = :name:',
            'bind' => [
                'name' => $group,
            ],
        ]);

        if ($group_id) {

            return $this->JsonResponse([$group_id->getId()]);
        } else {
            return $this->JsonResponse([0]);
        }
    }
}

