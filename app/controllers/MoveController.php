<?php

use Phalcon\Mvc\View;
use Phalcon\Paginator\Adapter\Model as PaginatorModel;

class MoveController extends ControllerBase
{

	public function indexAction()
	{
		$currentPage = $this->dispatcher->getParam('page');
		$group = GroupStore::find([
			'columns' => 'group_id, year',
			'group'   => 'group_id, year',
		]);

		$Groups = Group::find();
		$temp   = [];
		foreach ( $Groups as $g ) {
			$temp[$g->getId()] = $g;
		}
		$paginator   = new PaginatorModel(
			[
				"data"  => $group,
				"limit" => 3,
				"page"  => $currentPage,
			]
		);
		$page = $paginator->getPaginate();
		$this->view->setVars([
			'page' => $page,
			'names' => $temp,
		]);

	}

	public function delAction()
	{
		$year     = $this->dispatcher->getParam('year');
		$group_id = $this->dispatcher->getParam('group_id');
		$groups   = GroupStore::find([
			'conditions' => 'group_id = :group_id: and year = :year: ',
			'bind'       => [
				'group_id' => $group_id,
				'year'     => $year,
			]]);
		foreach ( $groups as $value ) {
			$value->delete();
		}

		return $this->response->redirect('/move', true);

	}

	public function editAction()
	{

		$year     = $this->dispatcher->getParam('year');
		$group_id = $this->dispatcher->getParam('group_id');
		$group    = GroupStore::findFirst([
			'conditions' => 'group_id = :group_id: and year = :year: ',
			'bind'       => [
				'group_id' => $group_id,
				'year'     => $year,
			]]);

		$groups = GroupStore::find([
			'conditions' => 'group_id = :group_id: and year = :year: ',
			'bind'       => [
				'group_id' => $group_id,
				'year'     => $year,
			]]);

		$form = new MoveForm($group);

		$this->view->setVars([
			'form'  => $form,
			'group' => $group,
		]);

		if ( !$this->request->isPost() ) {
			return $this->view;
		}
		if ( $form->isValid($this->request->getPost(), $group) ) {
			$group_id = $this->request->getPost('group_id');
			$year     = $this->request->getPost('year');
			$status   = $this->request->getPost('status');
			foreach ( $groups as $value ) {
				$value->setGroupId($group_id)->setYear($year)->setStatus($status);
				$value->save();
			}

			return $this->response->redirect('/move', true);
		}
	}

	public function studentsAction()
	{
		$year     = $this->dispatcher->getParam('year');
		$group_id = $this->dispatcher->getParam('group_id');
		$group    = Group::findFirst($group_id);
		$students = $this->modelsManager->createBuilder()
			->columns(['u.name', 'u.id'])
			->addFrom(Users::class, 'u')
			->innerJoin(GroupStore::class, 'u.id = gs.user_id', 'gs')
			->where("gs.group_id = :group_id: and gs.year = :year:", [
				'group_id' => $group_id,
				'year'     => $year,
			])
			->getQuery()
			->execute()
			->toArray();
		$this->view->setVars([
			'students' => $students,
			'group'    => $group->getName(),
		]);
	}

	public function studentAction()
	{
		$id = $this->dispatcher->getParam('id');

		$form  = new MoveStudentForm();
		$group = new GroupStore();

		$student = Users::findFirst($id);

		$this->view->setVars([
			'form'    => $form,
			'group'   => $group,
			'student' => $student,
		]);

		if ( !$this->request->isPost() ) {
			return $this->view;
		}

		if ( $form->isValid($this->request->getPost(), $group) ) {

			$empty = GroupStore::findFirst([
				'conditions' => 'user_id = :user_id: and group_id = :group_id: and year = :year:',
				'bind'       => [
					'user_id'  => $id,
					'group_id' => $this->request->getPost('group_id'),
					'year'     => $this->request->getPost('year'),
				],
			]);
			if (!$empty){
				if ( $group->create() ) {
					return $this->response->redirect('/move', true);
				}
			}
			else{
				$this->view->setVar('error','Такой ученик в этой группе уже существует!');
				return $this->view;
			}
		}
	}

}

