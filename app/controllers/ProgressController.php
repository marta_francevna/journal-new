<?php

use Phalcon\Mvc\View;

class ProgressController extends ControllerBase
{

    public function indexAction()
    {
    }

    public function addAction()
    {

        $form = new ProgressForm();
        $progress = new Progress();

        $this->view->setVars([
            'form' => $form,
            'progress' => $progress,
        ]);

        if (!$this->request->isPost()) {
            return $this->view;
        }

        if ($form->isValid($this->request->getPost(), $progress)) {

            if ($progress->create()) {
                $user = $this->session->get("user_id");
                $course = $this->session->get("course");
                $semester = $this->session->get("semester");
                $url = ('/student/' . $user . '/course/' . $course . '/semester/' . $semester);

                return $this->response->redirect($url, true);
            }
        }
    }

    public function editAction()
    {
        $id = $this->dispatcher->getParam('id');

        $progress = Progress::findFirst([
            'conditions' => 'id = :id: ',
            'bind' => [
                'id' => $id,
            ]]);

        $form = new ProgressForm($progress);
        $this->view->setVars([
            'form' => $form,
            'progress' => $progress,
        ]);
        if (!$this->request->isPost()) {
            return $this->view;
        }
        if ($form->isValid($this->request->getPost(), $progress)) {

            if ($progress->save()) {
                $user = $this->session->get("user_id");
                $course = $this->session->get("course");
                $semester = $this->session->get("semester");
                $url = ('/student/' . $user . '/course/' . $course . '/semester/' . $semester);

                return $this->response->redirect($url, true);
            }
        }
    }

    public function delAction()
    {
        $id = $this->dispatcher->getParam('id');
        $progress = Progress::findFirst([
            'conditions' => 'id = :id: ',
            'bind' => [
                'id' => $id,
            ]]);
        $progress->delete();
        $url = $this->request->getPost('url');

        return $this->response->redirect($url, true);
    }

    public function createAction()
    {

        $students = GroupStore::find([
            'conditions' => 'group_id = :group_id: and year = :year:',
            'bind' => [
                'group_id' => $this->session->get('group_id'),
                'year' => $this->session->get('year'),
            ],
            "columns" => "user_id",
            'group' => 'user_id',
        ]);
        $group = Group::findFirst($this->session->get('group_id'));

        $names = Users::find();
        $temp = [];
        foreach ($names as $name) {
            $temp[$name->getId()] = $name;
        }

        $this->view->setVars([
            'students' => $students,
            'names' => $temp,
        ]);

        $form = new ProgressCreateForm();
        $progress = new Progress();

        $this->view->setVars([
            'form' => $form,
            'progress' => $progress,
            'group' => $group,
        ]);

        if (!$this->request->isPost()) {
            return $this->view;
        }

        if ($form->isValid($this->request->getPost(), $progress)) {

            if ($progress->create()) {
                return $this->response->redirect('/info', true);
            }
        }
    }

    public function selectAction()
    {

        $student_id = $this->request->getPost('student_id');
        $group_store_id = GroupStore::findFirst([
            'conditions' => 'user_id = :user_id: and group_id = :group_id: and year = :year:',
            'bind' => [
                'user_id' => $student_id,
                'group_id' => $this->session->get('group_id'),
                'year' => $this->session->get('year'),
            ]]);

        if ($group_store_id) {

            return $this->JsonResponse([$group_store_id->getId()]);
        } else {
            return $this->JsonResponse([0]);
        }
    }

}

