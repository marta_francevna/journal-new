<?php

use Phalcon\Mvc\View;

class FilesController extends ControllerBase
{

    public function studentAction()
    {
        $id = $this->dispatcher->getParam('id');
        $status = $this->request->getPost('status');
        $name = $this->request->getPost('name');
        $groups = $this->modelsManager->createBuilder()
            ->columns(['g.name', 'gs.year', 'p.semester', 'g.id'])
            ->addFrom(Users::class, 'u')
            ->innerJoin(GroupStore::class, 'u.id = gs.user_id', 'gs')
            ->innerJoin(Group::class, 'g.id = gs.group_id', 'g')
            ->innerJoin(Progress::class, 'gs.id = p.group_store_id', 'p')
            ->where("gs.user_id = :user_id:", [
                'user_id' => $id,
            ])
            ->groupBy(['g.name', 'gs.year', 'p.semester', 'g.id'])
            ->getQuery()
            ->execute()
            ->toArray();

        if ($status == 2) {
            $file = fopen(BASE_PATH . "/Выпускники.txt", "a+");
        } else {
            $file = fopen(BASE_PATH . "/Отчисленные.txt", "a+");
        }
        fwrite($file, $name . "\r\n");
        foreach ($groups as $value) {
            $group_name = $value['name'];
            $year = $value['year'];
            $semester = $value['semester'];
            fwrite($file, "Группа: $group_name Год: $year Семестр: $semester" . "\r\n");
            $prog = $this->modelsManager->createBuilder()
                ->columns(['s.name', 'p.grade', 'p.omission'])
                ->addFrom(Users::class, 'u')
                ->innerJoin(GroupStore::class, 'u.id = gs.user_id', 'gs')
                ->innerJoin(Group::class, 'g.id = gs.group_id', 'g')
                ->innerJoin(Progress::class, 'gs.id = p.group_store_id', 'p')
                ->innerJoin(Subject::class, 's.id = p.subject_id', 's')
                ->where("gs.user_id = :user_id: and gs.group_id = :group_id: and gs.year = :year: and p.semester = :semester:", [
                    'user_id' => $id,
                    'group_id' => $value['id'],
                    'year' => $value['year'],
                    'semester' => $value['semester']
                ])
                ->getQuery()
                ->execute()
                ->toArray();
            foreach ($prog as $p) {
                $subject = $p['name'];
                $grage = $p['grade'];
                $omission = $p['omission'];
                fwrite($file, "Предмет: $subject Средний балл: $grage Пропуски: $omission" . "\r\n");
            }

        }

        fclose($file);
        $student = Users::findFirst($id);
        if ($student->delete()) {
            return $this->response->redirect('/students', true);
        }

    }

    public function groupAction()
    {
        $group_id = $this->dispatcher->getParam('group_id');
        $year = $this->dispatcher->getParam('year');
        $group = Group::findFirst($group_id);
        $students = $this->modelsManager->createBuilder()
            ->columns(['u.name', 'u.id'])
            ->addFrom(Users::class, 'u')
            ->innerJoin(GroupStore::class, 'u.id = gs.user_id', 'gs')
            ->where("gs.group_id = :group_id: and gs.year = :year:", [
                'group_id' => $group_id,
                'year' => $year,
            ])
            ->getQuery()
            ->execute()
            ->toArray();
        $file = fopen(BASE_PATH . "/Выпускники.txt", "a+");
        foreach ($students as $student) {
            fwrite($file, $student['name'] . "\r\n");
            $groups = $this->modelsManager->createBuilder()
                ->columns(['g.name', 'gs.year', 'p.semester', 'g.id'])
                ->addFrom(Users::class, 'u')
                ->innerJoin(GroupStore::class, 'u.id = gs.user_id', 'gs')
                ->innerJoin(Group::class, 'g.id = gs.group_id', 'g')
                ->innerJoin(Progress::class, 'gs.id = p.group_store_id', 'p')
                ->where("gs.user_id = :user_id:", [
                    'user_id' => $student['id'],
                ])
                ->groupBy(['g.name', 'gs.year', 'p.semester', 'g.id'])
                ->getQuery()
                ->execute()
                ->toArray();
            foreach ($groups as $value) {
                $group_name = $value['name'];
                $year = $value['year'];
                $semester = $value['semester'];
                fwrite($file, "Группа: $group_name Год: $year Семестр: $semester" . "\r\n");
                $prog = $this->modelsManager->createBuilder()
                    ->columns(['s.name', 'p.grade', 'p.omission'])
                    ->addFrom(Users::class, 'u')
                    ->innerJoin(GroupStore::class, 'u.id = gs.user_id', 'gs')
                    ->innerJoin(Group::class, 'g.id = gs.group_id', 'g')
                    ->innerJoin(Progress::class, 'gs.id = p.group_store_id', 'p')
                    ->innerJoin(Subject::class, 's.id = p.subject_id', 's')
                    ->where("gs.user_id = :user_id: and gs.group_id = :group_id: and gs.year = :year: and p.semester = :semester:", [
                        'user_id' => $student['id'],
                        'group_id' => $value['id'],
                        'year' => $value['year'],
                        'semester' => $value['semester']
                    ])
                    ->getQuery()
                    ->execute()
                    ->toArray();
                foreach ($prog as $p) {
                    $subject = $p['name'];
                    $grage = $p['grade'];
                    $omission = $p['omission'];
                    fwrite($file, "Предмет: $subject Средний балл: $grage Пропуски: $omission" . "\r\n");
                }

            }
            $student = Users::findFirst($student['id']);
            $student->delete();
        }
        fclose($file);
        $groupStore = GroupStore::find([
            'conditions' => 'group_id = :group_id: and year = :year:',
            'bind' => [
                'group_id' => $group_id,
                'year' => $year,
            ],
        ]);
        foreach ($groupStore as $gs) {
            $gs->delete();
        }

        return $this->response->redirect('/move', true);
    }

}

