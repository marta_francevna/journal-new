<?php

class InfoController extends ControllerBase
{
    public function initialize()
    {
        $avrz = $this->session->get("authorization");
        if ($avrz === null) {

            $this->dispatcher->forward([
                'controller' => 'Index',
                'action' => 'Route404',
            ]);
        }
    }

    public function indexAction()
    {

        $login = $this->session->get("login");
        $user = Users::findFirst([
            'conditions' => 'login = :login: or number = :login:',
            'bind' => [
                'login' => $login,
            ]]);

        $avrz = $this->session->get("authorization");
        if ($avrz === 0) {
            $flag = true;
            $courses = $this->modelsManager->createBuilder()
                ->columns(['gs.id', 'g.course'])
                ->addFrom(GroupStore::class, 'gs')
                ->innerJoin(Group::class, 'g.id = gs.group_id', 'g')
                ->where("gs.user_id = :user_id:", [
                    'user_id' => $user->getId(),
                ])
                ->orderBy('g.course DESC')
                ->getQuery()
                ->execute()
                ->toArray();
            $notEmpty = true;
            $notEmptyProgress = true;
            if (empty($courses)) {
                $notEmpty = false;
            }
            if ($notEmpty) {
                $course = $this->modelsManager->createBuilder()
                    ->columns(['gs.id', 'g.course', 'g.sort'])
                    ->addFrom(GroupStore::class, 'gs')
                    ->innerJoin(Group::class, 'g.id = gs.group_id', 'g')
                    ->where("gs.user_id = :user_id:", [
                        'user_id' => $user->getId(),
                    ])
                    ->orderBy('g.course DESC')
                    ->getQuery()
                    ->execute()
                    ->toArray();

                $semester = Progress::find([
                        "columns" => "semester",
                        'conditions' => 'group_store_id = :group_store_id:',
                        'bind' => [
                            'group_store_id' => $course[0]['id'],
                        ],
                        'order' => "semester DESC",
                        "group" => "semester",
                    ]
                );
                if (count($semester) == 0) {
                    $notEmptyProgress = false;
                }
                if ($notEmptyProgress) {
                    $progress = Progress::find([
                        'conditions' => ' semester = :semester: and group_store_id = :group_store_id:',
                        'bind' => [
                            'semester' => $semester[0]['semester'],
                            'group_store_id' => $course[0]['id'],
                        ]]);

                    $Subjects = Subject::find();
                    $temp = [];
                    foreach ($Subjects as $sub) {
                        $temp[$sub->getId()] = $sub;
                    }
                    $this->view->setVars([
                        'progress' => $progress,
                        'subjects' => $temp,
                        'semester' => $semester[0]['semester'],
                        'course' => $course[0],
                    ]);
                }
            }

            $semester = $this->request->getPost('semester');
            $group_store_id = $this->request->getPost('course');
            $group_id = GroupStore::findFirst($group_store_id)->getGroupId();
            $group = Group::findFirst($group_id);
            if (!empty($semester)) {
                $flag = false;
                $progress = Progress::find([
                    'conditions' => 'semester = :semester: and group_store_id = :group_store_id:',
                    'bind' => [
                        'semester' => $semester,
                        'group_store_id' => $group_store_id,
                    ]]);
                $Subjects = Subject::find();
                $temp = [];
                foreach ($Subjects as $sub) {
                    $temp[$sub->getId()] = $sub;
                }

                $this->view->setVars([
                    'progress' => $progress,
                    'subjects' => $temp,
                    'semester' => $semester,
                    'course' => $group,
                ]);
            }

            $this->view->setVars([
                'courses' => $courses,
                'flag' => $flag,
                'notEmpty' => $notEmpty,
            ]);
        }
        if ($avrz === 1) {

            $group = Group::find([
                "columns" => "name",
                'group' => 'name',
            ]);
            $this->view->setVars([
                'group' => $group,
            ]);
        }
    }

    public function settingAction()
    {

        $login = $this->session->get("login");

        $user = Users::findFirst([
            'conditions' => 'login = :login:',
            'bind' => [
                'login' => $login,
            ]]);
        $form = new UsersForm($user);

        $this->view->setVars([
            'form' => $form,
            'user' => $user,
        ]);
        if (!$this->request->isPost()) {
            return $this->view;
        }
        if ($form->isValid($this->request->getPost(), $user)) {

            if ($user->save()) {
                $login = $this->request->getPost("login");
                $this->session->set("login", $login);

                return $this->response->redirect('/info');
            }
        }
    }

    public function semestersStudentAction()
    {

        $id_gs = $this->request->getPost('id_gs');
        $gs = GroupStore::findFirst($id_gs);
        $sort = Group::findFirst($gs->getGroupId());
        $semesters = Progress::find([
                "columns" => "semester",
                'conditions' => 'group_store_id = :group_store_id:',
                'bind' => [
                    'group_store_id' => $id_gs,
                ],
                'order' => "semester ASC",
                "group" => "semester",
            ]
        );

        if ($semesters) {
            return $this->JsonResponse([0 => $semesters, 1 => $sort->getSort()]);
        } else {
            return $this->JsonResponse([0]);
        }
    }

    public function semesterKuratorAction()
    {
        $group_name = $this->request->getPost('group');
        $year = $this->request->getPost('year');
        $this->session->set("year", $year);
        $group = Group::findFirst([
            'conditions' => 'name = :name:',
            'bind' => [
                'name' => $group_name,
            ]]);
        $this->session->set("group_id", $group->getId());

        $group_store = $this->modelsManager->createBuilder()
            ->columns(['gs.id'])
            ->addFrom(GroupStore::class, 'gs')
            ->innerJoin(Group::class, 'g.id = gs.group_id', 'g')
            ->where("gs.year = :year: and group_id = :group_id:", [
                'year' => $year,
                'group_id' => $group->getId(),
            ])
            ->getQuery()
            ->execute()
            ->toArray();
        $semesters = Progress::find([
                "columns" => "semester",
                'conditions' => 'group_store_id = :group_store_id:',
                'bind' => [
                    'group_store_id' => $group_store[0]['id'],
                ],
                'order' => "semester ASC",
                "group" => "semester",
            ]
        );

        return $this->JsonResponse([0 => $semesters]);
    }

    public function progressAction()
    {
        $group_name = $this->request->getPost('group');
        $semester = $this->request->getPost('sem');
        $year = $this->request->getPost('year');

        if (!$this->request->isPost()) {

            $this->dispatcher->forward([
                'controller' => 'Index',
                'action' => 'Route404',
            ]);
        } else {

            $group = Group::findFirst([
                'conditions' => 'name = :name:',
                'bind' => [
                    'name' => $group_name,
                ],
            ]);

            $progress = $this->modelsManager->createBuilder()
                ->columns(["gs.user_id"])
                ->addFrom(GroupStore::class, 'gs')
                ->where("gs.group_id = :group_id:", [
                    'group_id' => $group->getId(),
                ])
                ->andWhere('year = :year:', [
                    'year' => $year,
                ])
                ->groupBy('gs.user_id')
                ->getQuery()
                ->execute()
                ->toArray();

            $names = Users::find();
            $temp = [];
            foreach ($names as $name) {
                $temp[$name->getId()] = $name;
            }

            $this->view->setVars([
                'progress' => $progress,
                'names' => $temp,
                'semester' => $semester,
                'group' => $group,
                'year' => $year,
            ]);
        }
    }

    public function studentsAction()
    {
        $user_id = $this->dispatcher->getParam('user_id');
        $this->session->set("user_id", $user_id);
        $course = $this->dispatcher->getParam('course');
        $this->session->set("course", $course);
        $semester = $this->dispatcher->getParam('semester');
        $this->session->set("semester", $semester);
        $user = Users::findFirst($user_id);
        $year = $this->request->getPost('year');
        $group_id = $this->request->getPost('group_id');

        if ($user) {

            if (!empty($year)) {
                $gs_id = GroupStore::findFirst([
                    'conditions' => 'group_id = :group_id: and year = :year: and user_id = :user_id:',
                    'bind' => [
                        'group_id' => $group_id,
                        'year' => $year,
                        'user_id' => $user_id
                    ],
                ]);
                $this->session->set("group_store_id", $gs_id->getId());
            } else {
                $gs_id = GroupStore::findFirst([
                    'conditions' => 'group_id = :group_id: and year = :year: and user_id = :user_id:',
                    'bind' => [
                        'group_id' => $this->session->get('group_id'),
                        'year' => $this->session->get('year'),
                        'user_id' => $this->session->get('user_id'),
                    ],
                ]);
            }

            $progress = Progress::find([
                'conditions' => 'semester = :semester: and group_store_id = :group_store_id:',
                'bind' => [
                    'semester' => $semester,
                    'group_store_id' => $gs_id->getId(),
                ]]);
            $a = count($progress);
            if ($a > 0) {

                $Subjects = Subject::find();
                $temp = [];
                foreach ($Subjects as $sub) {
                    $temp[$sub->getId()] = $sub;
                }

                $this->view->setVars([
                    'progress' => $progress,
                    'subjects' => $temp,
                    'student' => $user->getName(),
                ]);
            }
            if ($a == 0) {
                $this->view->setVars([
                    'error' => 'Извините, нет успеваемости',
                ]);
            }
        } else {
            $this->dispatcher->forward([
                'controller' => 'Index',
                'action' => 'Route404',
            ]);
        }
    }


}

