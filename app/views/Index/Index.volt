<script type="text/javascript" src="/js/head.js"></script>

<header class="header">
	<div class="b-title-1">
		<span class="text">Электронный дневник</span>
	</div>
	<div class="b-title-2">
		<span class="text">Вход</span>
	</div>
</header>

<div class="main">
	<form method="post" action="/login" id="loginform" class="form-entry">
		<div id="resp1" class="error" style="display: none">Ошибка!</div>
		<div class="group">
			<input type="text" name="login" id="login" required>
			<span class="bar"></span>
			<label>Логин / № студенческого</label>
		</div>
		<div class="group">
			<input type="password" name="password" id="password" required>
			<span class="bar"></span>
			<label>Пароль</label>
		</div>
		<div class="b-button button-custom">
			<a href="#" class="link-back">На главную</a>
			<button type="submit" class="button">Войти</button>
		</div>
	</form>
</div>
