<script type="text/javascript" src="/js/create-progress.js"></script>
<header class="header">
	<div class="b-title-1">
		<span class="text">Электронный дневник</span>
	</div>
	<div class="b-title-2">
		<span class="text">Добавление успеваемости</span>
	</div>
</header>

<nav class="navbar">
	<div class="b-navbar container">
		<ul class="menu-items">
			<li><a href="/info" class="active">Основное</a></li>
			<li><a href="/subjects">Предметы</a></li>
			<li><a href="/students">Учащиеся</a></li>
			<li><a href="/groups">Группы</a></li>
			<li><a href="/move" >Переводы</a></li>
		</ul>
		<div class="b-login">
			<a href="/setting" class="login">{{ session.get('login') }}</a>
			<a href="/logout" class="exit">Выход</a>
		</div>
	</div>
</nav>

<div class="main">
	<form method="post" action="/progress/add" class="form-edit-inf">
		<div class="b-errors">
            {% for message in form.getMessages() %}
				<span class="errors">{{ message.getMessage() }}</span>
            {% endfor %}
		</div>
		<input type="hidden" name="group_store_id" value="{{ session.get('group_store_id') }}">
		<label>Предмет:</label>
		{{ form.render('subject_id') }}
		<label>Средний балл:</label>
		{{ form.render('grade') }}
		<label>Количество пропусков:</label>
		{{ form.render('omission') }}
		<input type="hidden" name="semester" value="{{ session.semester }}">
		<div class="b-button button-custom button-edit-inf">
			<a href="/student/{{ session.get('user_id') }}/course/{{ session.get('course') }}/semester/{{ session.get('semester') }}"
			   class="link-back">« Назад</a>
			<button type="submit" class="button">Добавить</button>
		</div>
	</form>
</div>

