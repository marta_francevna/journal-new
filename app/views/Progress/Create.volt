<script type="text/javascript" src="/js/create-progress.js"></script>

<header class="header">
    <div class="b-title-1">
        <span class="text">Электронный дневник</span>
    </div>
    <div class="b-title-2">
        <span class="text">Создание успеваемости</span>
    </div>
</header>

<nav class="navbar">
    <div class="b-navbar container">
        <ul class="menu-items">
            <li><a href="/info" class="active">Основное</a></li>
            <li><a href="/subjects">Предметы</a></li>
            <li><a href="/students">Учащиеся</a></li>
            <li><a href="/groups">Группы</a></li>
            <li><a href="/move">Переводы</a></li>
        </ul>
        <div class="b-login">
            <a href="/setting" class="login">{{ session.get('login') }}</a>
            <a href="/logout" class="exit">Выход</a>
        </div>
    </div>
</nav>

<div class="main">
    <div class="b-main" id="resp1">
        <form method="post" action="/progress/create" id="form">
            <div id="response" class="error" style="display: none">Ошибка!</div>
            <div class="b-button button-custom button-edit-inf">
                <a href="/info" class="link-back">« Назад</a>
            </div>
            <div>
                {% if students|length %}
                    <label>Выберите ученика:</label>
                    <select name="student" id="student"
                            onmousedown="$(':first-child', this).remove(); this.onmousedown = null;">
                        <option value=""></option>

                        {% for student in students %}
                            {% set sname = names[student.user_id] %}
                            <option value="{{ student.user_id }}">{{ sname.name }}</option>
                        {% endfor %}
                    </select>
                {% else %}
                    <p>Сначала добавьте учеников в эту группу</p>
                {% endif %}
            </div>
            <div class="b-errors">
                {% for message in form.getMessages() %}
                    <span class="errors">{{ message.getMessage() }}</span>
                {% endfor %}
            </div>
        </form>
        <div id="respon" style="display: none">
            <form action="/progress/create" method="post" class="form-edit-inf">

                <label>Предмет:</label>
                {{ form.render('subject_id') }}

                <input type="hidden" name="group_store_id" id="group_store_id">
                <div>
                    <label>Средний балл:</label>
                    {{ form.render('grade') }}
                </div>
                <div>
                    <label>Количество пропусков:</label>
                    {{ form.render('omission') }}
                </div>
                <div>
                    {% if group.sort ==1 %}
                        <label>Семестр:</label>
                    {% else %}
                        <label>Полугодие:</label>
                    {% endif %}
                    {{ form.render('semester') }}
                </div>
                <div class="b-button button-custom button-edit-inf">
                    <button type="submit" class="button">Создать</button>
                </div>
            </form>
        </div>
    </div>
</div>
