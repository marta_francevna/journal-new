<header class="header">
    <div class="b-title-1">
        <span class="text">Электронный дневник</span>
    </div>
    <div class="b-title-2">
        <span class="text">Группы</span>
    </div>
</header>

<nav class="navbar">
    <div class="b-navbar container">
        <ul class="menu-items">
            <li><a href="/info">Основное</a></li>
            <li><a href="/subjects">Предметы</a></li>
            <li><a href="/students">Учащиеся</a></li>
            <li><a href="/groups" class="active">Группы</a></li>
            <li><a href="/move" >Переводы</a></li>
        </ul>
        <div class="b-login">
            <a href="/setting" class="login">{{ session.get('login') }}</a>
            <a href="/logout" class="exit">Выход</a>
        </div>
    </div>
</nav>

<div class="main">
    <div class="b-main">

        {% if page.items|length>0 %}
        <form class="search-form" method="post" action="/groups">
            <input class="search-input" type="text" name="search">
            <button class="search-button" name="submit"></button>
        </form>
        <div class="b-button button-custom">
            <a href="/groups" class="link-back">Обновить</a>
            <a href="/groups/add" class="button">Добавить группу</a>
        </div>
        <table>
            <thead>
            <tr>
                <th>Группа</th>
                <th>Вид</th>
                <th>Курс</th>
                <th>Изм.</th>
                <th>Уд.</th>
            </tr>
            </thead>
            <tbody>

            {% for item in page.items %}
                <tr>
                    <td>{{ item.name }}</td>
	                {% if item.sort == 0 %}
                        <td>ПТО</td>
	                {% else %}
                        <td>ССО</td>
	                {% endif %}
                    <td>{{ item.course }}</td>
                    <td><a href="/groups/edit/{{ item.getId() }}" class="edit"></a></td>
                    <td><a href="/groups/del/{{ item.getId() }}" class="delete"></a></td>
                </tr>
            {% endfor %}

            </tbody>
        </table>

        {% if page.total_pages > 1 %}
            <div class="numbers">
                <a class="prev page-numbers" href="/groups/page/{{ page.before }}">«</a>
                {% set reserve =  3 %}
                {% set count =  7 %}
                {% set startP = 1 %}
                {% set countP = (page.last - (count + startP))>0 ? (count + startP) : page.last %}

                {% if   page.current - reserve > 0 %}
                    {% set startP = page.current - reserve %}
                    {% set countP = (page.last - (count + startP))>0 ? count + startP : page.last %}
                {% endif %}

                {% if countP == 0 %}
                    {% set countP = 1 %}
                {% endif %}

                {% if   page.current > reserve + 1 %}
                    <a class="page-numbers" href="/groups/page/1">1</a>
                    <span aria-hidden="true">...</span>
                {% endif %}

                {#body#}
                {% for i in startP..countP %}
                    <a {{ (i==page.current)?'class="page-numbers current"':'page-numbers"' }}
                            class="page-numbers" href="/groups/page/{{ i }}">{{ i }}</a>
                {% endfor %}

                {#footer#}
                {% if   page.last - startP > count %}
                    <span aria-hidden="true">...</span>
                    <a {{ (i==page.current)?'class="page-numbers current"':'page-numbers"' }}
                            class="page-numbers" href="/groups/page/{{ page.last }}">{{ page.last }}</a>
                {% endif %}

                <a class="next page-numbers" href="/groups/page/{{ page.next }}">»</a>
            </div>
        {% endif %}

    </div>
</div>

{% else %}
    <div class="b-button button-custom">
        <a href="/groups" class="link-back">Обновить</a>
        <a href="/groups/add" class="button">Добавить группу</a>
    </div>
    <div class="b-text">
        <span class="text">Извините, ничего не найдено. =(</span>
    </div>
{% endif %}

