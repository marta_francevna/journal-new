<script type="text/javascript" src="/js/student-semestr.js"></script>
<script type="text/javascript" src="/js/kurator-course.js"></script>

{% if session.get('authorization') === 0 %}
    <header class="header">
        <div class="b-title-2">
            <span class="text">Электронный дневник</span>
        </div>
    </header>

    <nav class="navbar">
        <div class="b-navbar container">
            <div class="b-login">
                <a href="/setting" class="login">{{ session.get('login') }}</a>
                <a href="/logout" class="exit">Выход</a>
            </div>
        </div>
    </nav>

{% elseif session.get('authorization') === 1 %}
    <header class="header">
        <div class="b-title-1">
            <span class="text">Электронный дневник</span>
        </div>
        <div class="b-title-2">
            <span class="text">Основное</span>
        </div>
    </header>

    <nav class="navbar">
        <div class="b-navbar container">
            <ul class="menu-items">
                <li><a href="/info" class="active">Основное</a></li>
                <li><a href="/subjects">Предметы</a></li>
                <li><a href="/students">Учащиеся</a></li>
                <li><a href="/groups">Группы</a></li>
                <li><a href="/move">Переводы</a></li>
            </ul>
            <div class="b-login">
                <a href="/setting" class="login"> {{ session.get('login') }}</a>
                <a href="/logout" class="exit">Выход</a>
            </div>
        </div>
    </nav>
{% endif %}

{% if session.get('authorization') === 0 %}
    {% if notEmpty %}
        <div class="main">
            <div class="b-main">
                <form method="post" action="/info" id="form">
                    <div id="resp" class="error" style="display: none"></div>
                    <div>
                        <label>Выберите курс:</label>
                        <select name="course" id="course"
                                onmousedown="$(':first-child', this).remove(); this.onmousedown = null;">
                            <option value=""></option>

                            {% for c in courses %}
                                <option value="{{ c['id'] }}">{{ c['course'] }}</option>
                            {% endfor %}
                        </select>
                    </div>
                    <div id="resp1"></div>
                    <input type="hidden" name="course_number" id="course_number"/>
                </form>

                {% if  progress|length and flag===false %}
                    <div class="b-text">
                        <span class="text"> Курс: {{ course.course }}.
                            {% if course.sort==1 %}
                                Семестр: {{ semester }}
                            {% else %}
                                Полугодие: {{ semester }}
                            {% endif %}
                        </span>
                    </div>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Предмет</th>
                            <th>Ср. оценка</th>
                            <th>Пропусков</th>
                        </tr>
                        </thead>
                        <tbody>

                        {% for p in progress %}
                            {% set sub = subjects[p.subject_id] %}
                            <tr>
                                <td>{{ sub.name }}</td>
                                <td>{{ p.grade }}</td>
                                <td>{{ p.omission }}</td>
                            </tr>
                        {% endfor %}

                        </tbody>
                    </table>
                {% endif %}

                {% if  progress|length and flag %}
                    <div class="b-text">
						<span class="text">Последнее -  Курс: {{ course['course'] }}.
                            {% if course['sort']==1 %}
                                Семестр: {{ semester }}
                            {% else %}
                                Полугодие: {{ semester }}
                            {% endif %}
						</span>
                    </div>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Предмет</th>
                            <th>Ср. оценка</th>
                            <th>Пропусков</th>
                        </tr>
                        </thead>
                        <tbody>

                        {% for p in progress %}
                            {% set sub = subjects[p.subject_id] %}
                            <tr>
                                <td>{{ sub.name }}</td>
                                <td>{{ p.grade }}</td>
                                <td>{{ p.omission }}</td>
                            </tr>
                        {% endfor %}

                        </tbody>
                    </table>
                {% endif %}

            </div>
        </div>
    {% else %}
        <div class="main">
            <div class="b-main">
                <p>Извините,нет данных</p>
            </div>
        </div>
    {% endif %}
{% endif %}


{% if session.get('authorization') === 1 %}
    <div class="main">
        <div class="b-main">
            <form method="post" action="/progress" id="form2" class="form-progress">
                <div class="b-button button-custom" style="display: none" id="create">
                    <a href="/progress/create/" class="button">Создать успеваемость</a>
                </div>
                <div style="display: inline-block" id="resp1">
                    <label>Выберите группу:</label>
                    <select name="group" id="group"
                            onmousedown="$(':first-child', this).remove(); this.onmousedown = null;">
                        <option value=0></option>

                        {% for g in group %}
                            <option>{{ g.name }}</option>
                        {% endfor %}

                    </select>
                    <input name="year" id="year" type="number" min="1900" max="2099" step="1" value="2017"/>
                    <div class="b-button i">
                        <input class="button" type="button" id="b1" value="Далее">
                    </div>
                </div>
                <div id="resp2"></div>
                <div id="resp3"></div>
                <div id="response" class="error" style="display: none"></div>
            </form>
        </div>
    </div>
{% endif %}


