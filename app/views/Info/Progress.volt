<header class="header">
    <div class="b-title-1">
        <span class="text">Электронный дневник</span>
    </div>
    <div class="b-title-2">
        <span class="text">Основное</span>
    </div>
</header>

<nav class="navbar">
    <div class="b-navbar container">
        <ul class="menu-items">
            <li><a href="/info" class="active">Основное</a></li>
            <li><a href="/subjects">Предметы</a></li>
            <li><a href="/students">Учащиеся</a></li>
            <li><a href="/groups">Группы</a></li>
            <li><a href="/move">Переводы</a></li>
        </ul>
        <div class="b-login">
            <a href="/setting" class="login">{{ session.get('login') }}</a>
            <a href="/logout" class="exit">Выход</a>
        </div>
    </div>
</nav>

<div class="main">
    <div class="b-main">
        <div class="b-button button-custom">
            <a href="/info" class="link-back">« Назад</a>
            <a href="/progress/create/" class="button">Создать успеваемость</a>
        </div>
        <div class="b-text">
            <span class="text">Группа: {{ group.getName() }}. Курс: {{ group.getCourse() }}.
                {% if group.sort == 1 %}
                Семестр: {{ semester }}
                {% else %}
                Полугодие: {{ semester }}
            {% endif %}</span>
        </div>
        <table>
            <thead>
            <tr>
                <th>ФИО</th>
                <th>Показ.</th>
            </tr>
            </thead>
            <tbody>

            {% for p in progress %}
                {% set student = names[p['user_id']] %}
                <tr>
                    <td>{{ student.name }}</td>
                    <td><a href="/student/{{ p['user_id'] }}/course/{{ group.getCourse() }}/semester/{{ semester }}"
                           class="book" onclick="document.getElementById('form-{{ p['user_id'] }}').submit(); return false;"></a></td>
                </tr>
                <form method="post" id="form-{{ p['user_id'] }}"
                      action="/student/{{ p['user_id'] }}/course/{{ group.getCourse() }}/semester/{{ semester }}">
                    <input type="hidden" name="year" value="{{ year }}"/>
                    <input type="hidden" name="group_id" value="{{ group.getId() }}"/>
                </form>
            {% endfor %}

            </tbody>
        </table>
    </div>
</div>

