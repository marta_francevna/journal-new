<header class="header">
    <div class="b-title-1">
        <span class="text">Электронный дневник</span>
    </div>
    <div class="b-title-2">
        <span class="text">Редактирование профиля</span>
    </div>
</header>

<nav class="navbar">
    <div class="b-navbar container">
        <div class="b-login">
            <a href="/setting" class="login">{{ session.get('login') }}</a>
            <a href="/logout" class="exit">Выход</a>
        </div>
    </div>
</nav>

<div class="main">
    <form method="post" action="/setting" id="loginform" class="form-entry">
        <div class="b-errors">
            {% for message in form.getMessages() %}
                <span class="errors">{{ message.getMessage() }}</span>
            {% endfor %}
        </div>
        <div class="group">
            <input type="text" name="login" id="login" value="{{ user.login }}" required>
            <span class="bar"></span>
            <label>Новый логин</label>
        </div>
        <div class="group">
            <input type="password" name="password" id="password" required>
            <span class="bar"></span>
            <label>Новый пароль</label>
        </div>
        <div class="b-button button-custom">
            <a href="/info" class="link-back">« Назад</a>
            <button type="submit" class="button">Сохранить профиль</button>
        </div>
    </form>
</div>

