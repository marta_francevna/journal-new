<header class="header">
	<div class="b-title-1">
		<span class="text">Электронный дневник</span>
	</div>
	<div class="b-title-2">
		<span class="text">Перевод ученика {{ student.getName() }}</span>
	</div>
</header>

<div class="main">
	<form method="post" action="/move/student/{{ student.getId() }}"  class="form-edit-inf">
		<input type="hidden" name="user_id" value="{{ student.getId() }}">
		<label>Группа</label>
		{{ form.render('group_id') }}
		<label>Год:</label>
		{{ form.render('year') }}
		{{ form.render('status') }}
		<div class="b-button button-custom button-edit-inf">
			<a href="/move" class="link-back">« Назад</a>
			<button type="submit" class="button">Сохранить</button>
		</div>
		<span class="errors">
			{% for message in form.getMessages() %}
				<p>{{ message.getMessage() }}</p>
			{% endfor %}
			{% if error!==null %}
			{{ error }}
			{% endif %}
		</span>
	</form>
</div>