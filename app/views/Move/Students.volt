{% if session.get('authorization') === 0 %}
	<header class="header">
		<div class="b-title-2">
			<span class="text">Электронный дневник</span>
		</div>
	</header>

	<nav class="navbar">
		<div class="b-navbar container">
			<div class="b-login">
				<a href="/setting" class="login">{{ session.get('login') }}</a>
				<a href="/logout" class="exit">Выход</a>
			</div>
		</div>
	</nav>

{% elseif session.get('authorization') === 1 %}
	<header class="header">
		<div class="b-title-1">
			<span class="text">Электронный дневник</span>
		</div>
		<div class="b-title-2">
			<span class="text">Ученики группы {{ group }}</span>
		</div>
	</header>

	<nav class="navbar">
		<div class="b-navbar container">
			<ul class="menu-items">
				<li><a href="/info">Основное</a></li>
				<li><a href="/subjects">Предметы</a></li>
				<li><a href="/students">Учащиеся</a></li>
				<li><a href="/groups">Группы</a></li>
				<li><a href="/move" class="active">Переводы</a></li>
			</ul>
			<div class="b-login">
				<a href="/setting" class="login"> {{ session.get('login') }}</a>
				<a href="/logout" class="exit">Выход</a>
			</div>
		</div>
	</nav>
{% endif %}

<div class="main">
	<div class="b-main">

		{% if students|length %}
		<table>
			<thead>
				<tr>
					<th>ФИО</th>
				</tr>
			</thead>
			<tbody>
				{% for student in students %}
					<tr>
						<td><a href="/move/student/{{ student['id'] }}">{{ student['name'] }}</a> </td>
					</tr>
				{% endfor %}
			</tbody>
		</table>
		<div class="b-button button-custom button-edit-inf">
			<a href="/move" class="link-back">« Назад</a>
		</div>
	</div>
</div>

{% else %}
	<div class="b-text">
		<span class="text">Извините, ничего не найдено. =(</span>
	</div>
{% endif %}