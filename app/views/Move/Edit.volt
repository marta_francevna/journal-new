<header class="header">
	<div class="b-title-1">
		<span class="text">Электронный дневник</span>
	</div>
	<div class="b-title-2">
		<span class="text">Редактирование группы</span>
	</div>
</header>

<div class="main">
	<form method="post" onsubmit="send(this);" class="form-edit-inf">
		<label>Группа</label>
		{{ form.render('group_id') }}
		<label>Год:</label>
		{{ form.render('year') }}
		<label>Статус</label>
		{{ form.render('status') }}
		<div class="b-button button-custom button-edit-inf">
			<a href="/move" class="link-back">« Назад</a>
			<button type="submit" class="button">Сохранить</button>
		</div>
		<span class="errors">
			{% for message in form.getMessages() %}
				<p>{{ message.getMessage() }}</p>
			{% endfor %}
		</span>
	</form>
</div>
<script>
	function send(form) {
		var status = document.getElementById("status").value;
		if ( status == 0 ) {
			form.action = "/move/edit/{{ group.getGroupId() }}/{{ group.getYear() }}";
		} else {
			form.action = "/files/group/{{ group.getGroupId() }}/{{ group.getYear() }}";
		}
	}
</script>
