{% if session.get('authorization') === 0 %}
	<header class="header">
		<div class="b-title-2">
			<span class="text">Электронный дневник</span>
		</div>
	</header>

	<nav class="navbar">
		<div class="b-navbar container">
			<div class="b-login">
				<a href="/setting" class="login">{{ session.get('login') }}</a>
				<a href="/logout" class="exit">Выход</a>
			</div>
		</div>
	</nav>

{% elseif session.get('authorization') === 1 %}
	<header class="header">
		<div class="b-title-1">
			<span class="text">Электронный дневник</span>
		</div>
		<div class="b-title-2">
			<span class="text">Переводы</span>
		</div>
	</header>

	<nav class="navbar">
		<div class="b-navbar container">
			<ul class="menu-items">
				<li><a href="/info">Основное</a></li>
				<li><a href="/subjects">Предметы</a></li>
				<li><a href="/students">Учащиеся</a></li>
				<li><a href="/groups">Группы</a></li>
				<li><a href="/move" class="active">Переводы</a></li>
			</ul>
			<div class="b-login">
				<a href="/setting" class="login"> {{ session.get('login') }}</a>
				<a href="/logout" class="exit">Выход</a>
			</div>
		</div>
	</nav>
{% endif %}

<div class="main">
	<div class="b-main">

		{% if page.items|length>0 %}
		<div class="b-button button-custom">
			<a href="/move" class="link-back">Обновить</a>
		</div>
		<table>
			<thead>
				<tr>
					<th>Группа</th>
					<th>Год</th>
					<th>Изм.</th>
					<th>Уд.</th>
				</tr>
			</thead>
			<tbody>
				{% for item in page.items %}
					{% set group = names[item.group_id] %}
				<tr>
					<td><a href="/move/students/{{ item.group_id }}/{{ item.year }}">{{ group.name }}</a> </td>
					<td>{{ item.year }}</td>
					<td><a href="/move/edit/{{ item.group_id }}/{{ item.year }}" class="edit"></a></td>
					<td><a href="/move/del/{{ item.group_id }}/{{ item.year }}" class="delete"></a></td>
				</tr>
				{% endfor %}
			</tbody>
		</table>

		{% if page.total_pages > 1 %}
			<div class="numbers">
				<a class="prev page-numbers" href="/move/page/{{ page.before }}">«</a>
				{% set reserve =  3 %}
				{% set count =  7 %}
				{% set startP = 1 %}
				{% set countP = (page.last - (count + startP))>0 ? (count + startP) : page.last %}

				{% if   page.current - reserve > 0 %}
					{% set startP = page.current - reserve %}
					{% set countP = (page.last - (count + startP))>0 ? count + startP : page.last %}
				{% endif %}

				{% if countP == 0 %}
					{% set countP = 1 %}
				{% endif %}

				{% if   page.current > reserve + 1 %}
					<a class="page-numbers" href="/move/page/1">1</a>
					<span aria-hidden="true">...</span>
				{% endif %}

				{#body#}
				{% for i in startP..countP %}
					<a {{ (i==page.current)?'class="page-numbers current"':'page-numbers"' }} class="page-numbers" href="/move/page/{{ i }}">{{ i }}</a>
				{% endfor %}

				{#footer#}
				{% if   page.last - startP > count %}
					<span aria-hidden="true">...</span>
					<a {{ (i==page.current)?'class="page-numbers current"':'page-numbers"' }} class="page-numbers" href="/move/page/{{ page.last }}">{{ page.last }}</a>
				{% endif %}

				<a class="next page-numbers" href="/move/page/{{ page.next }}">»</a>
			</div>
		{% endif %}

	</div>
</div>

{% else %}
	<div class="b-button button-custom">
		<a href="/move" class="link-back">Обновить</a>
	</div>
	<div class="b-text">
		<span class="text">Извините, ничего не найдено. =(</span>
	</div>
{% endif %}