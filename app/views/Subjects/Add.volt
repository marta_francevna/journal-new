<header class="header">
    <div class="b-title-1">
        <span class="text">Электронный дневник</span>
    </div>
    <div class="b-title-2">
        <span class="text">Добавление предмета</span>
    </div>
</header>

<nav class="navbar">
    <div class="b-navbar container">
        <ul class="menu-items">
            <li><a href="/info">Основное</a></li>
            <li><a href="/subjects" class="active">Предметы</a></li>
            <li><a href="/students" >Учащиеся</a></li>
            <li><a href="/groups">Группы</a></li>
            <li><a href="/move" >Переводы</a></li>
        </ul>
        <div class="b-login">
            <a href="/setting" class="login">{{ session.get('login') }}</a>
            <a href="/logout" class="exit">Выход</a>
        </div>
    </div>
</nav>

<div class="main">
    <form method="post" action="/subjects/add" class="form-edit-inf">
        <div class="b-errors">
            {% for message in form.getMessages() %}
                <span class="errors">{{ message.getMessage() }}</span>
            {% endfor %}
        </div>
        <label>Название:</label>
        {{ form.render('name') }}
        <div class="b-button button-custom button-edit-inf">
            <a href="/subjects" class="link-back">« Назад</a>
            <button type="submit" class="button">Создать</button>
        </div>
    </form>
</div>
