<header class="header">
    <div class="b-title-1">
        <span class="text">Электронный дневник</span>
    </div>
    <div class="b-title-2">
        <span class="text">Редактирование предмета</span>
    </div>
</header>

<div class="main">
    <form method="post" action="/subjects/edit/{{ subjects.getId() }}" class="form-edit-inf">
        <div class="b-errors">
            {% for message in form.getMessages() %}
                <span class="errors">{{ message.getMessage() }}</span>
            {% endfor %}
        </div>
        <label>Название предмета:</label>
        {{ form.render('name') }}
        <div class="b-button button-custom button-edit-inf">
            <a href="/subjects" class="link-back">« Назад</a>
            <button type="submit" class="button">Сохранить</button>
        </div>
    </form>
</div>

