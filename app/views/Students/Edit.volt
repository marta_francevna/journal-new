<header class="header">
	<div class="b-title-1">
		<span class="text">Электронный дневник</span>
	</div>
	<div class="b-title-2">
		<span class="text">Редактирование ученика</span>
	</div>
</header>

<div class="main">
	<form method="post"  onsubmit = "send(this);"  class="form-edit-inf">
		{{ formGS.render('user_id') }}
		<label>Группа:</label>
		{{ formGS.render('group_id') }}
		<label>Год:</label>
		{{ formGS.render('year') }}
		{{ formGS.render('status') }}
		<label>ФИО:</label>
		{{ form.render('name') }}
		<label>Логин</label>
		{{ form.render('login') }}
		<label>Пароль</label>
		{{ form.render('password') }}
		<label>Студенческий</label>
		{{ form.render('number') }}
		{{ form.render('role') }}
		<label>Статус</label>
		{{ form.render('status') }}
		<div class="b-button button-custom button-edit-inf">
			<a href="/students" class="link-back">« Назад</a>
			<button type="submit" class="button">Сохранить</button>
		</div>
		<span class="errors">
			{% for message in form.getMessages() %}
				<p>{{ message.getMessage() }}</p>
			{% endfor %}
			{% set messages = student.getMessages() %}
			{% if messages !== null %}
				{% for message in student.getMessages() %}
					<span class="errors">{{ message.getMessage() }}</span>
				{% endfor %}
			{% endif %}
		</span>
	</form>
</div>
<script>
	function send(form) {
		var status = document.getElementById("status").value;
		if (status==0){
			form.action = "/students/edit/{{ group_id }}/{{ student.getId() }}";
		}else {
			form.action = "/files/student/{{ student.getId() }}";
		}
	}
</script>

