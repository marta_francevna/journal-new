<header class="header">
	<div class="b-title-1">
		<span class="text">Электронный дневник</span>
	</div>
	<div class="b-title-2">
		<span class="text">Учащиеся группы {{ group }}</span>
	</div>
</header>

<nav class="navbar">
	<div class="b-navbar container">
		<ul class="menu-items">
			<li><a href="/info">Основное</a></li>
			<li><a href="/subjects">Предметы</a></li>
			<li><a href="/students" class="active">Учащиеся</a></li>
			<li><a href="/groups">Группы</a></li>
			<li><a href="/move" >Переводы</a></li>
		</ul>
		<div class="b-login">
			<a href="/setting" class="login">{{ session.get('login') }}</a>
			<a href="/logout" class="exit">Выход</a>
		</div>
	</div>
</nav>
{% if students|length %}
<div class="main">
	<div class="b-main">

		<div class="b-button button-custom">
			<a href="/students" class="link-back">Обновить</a>
			<a href="/students/add" class="button">Добавить</a>
		</div>

		<table>
			<thead>
				<tr>
					<th>ФИО</th>
					<th>Год</th>
					<th>№ студ.</th>
					<th>Статус</th>
					<th>Изм.</th>
					<th>Уд.</th>
				</tr>
			</thead>
			<tbody>

				{% for item in students %}
					<tr>
						<td>{{ item['name'] }}</td>
						<td>{{ item['year'] }}</td>
						<td>{{ item['number'] }}</td>
						{% if item['status'] == 0 %}
							<td>Учится</td>
						{% else %}
							<td>Переведен</td>
						{% endif %}
						<td><a href="/students/edit/{{ item['group_id'] }}/{{ item['id'] }}" class="edit"></a></td>
						<td><a href="/students/del/{{ item['id'] }}" class="delete"></a></td>
					</tr>
				{% endfor %}

			</tbody>

		</table>

	</div>
</div>
{% else %}
<div class="main">
    <div class="b-main">
    <div class="b-button button-custom">
        <a href="/students" class="link-back">Обновить</a>
        <a href="/students/add" class="button">Добавить ученика</a>
    </div>
    <div class="b-text">
        <span class="text">Извините, ничего не найдено. =(</span>
    </div>
    </div>
</div>
{% endif %}

