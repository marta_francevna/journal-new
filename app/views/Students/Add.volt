<script type="text/javascript" src="/js/create-user.js"></script>
<header class="header">
	<div class="b-title-1">
		<span class="text">Электронный дневник</span>
	</div>
	<div class="b-title-2">
		<span class="text">Добавление учащегося</span>
	</div>
</header>

<nav class="navbar">
	<div class="b-navbar container">
		<ul class="menu-items">
			<li><a href="/info">Основное</a></li>
			<li><a href="/subjects">Предметы</a></li>
			<li><a href="/students" class="active">Учащиеся</a></li>
			<li><a href="/groups">Группы</a></li>
			<li><a href="/move" >Переводы</a></li>
		</ul>
		<div class="b-login">
			<a href="/setting" class="login">{{ session.get('login') }}</a>
			<a href="/logout" class="exit">Выход</a>
		</div>
	</div>
</nav>

<div class="main">
	<div class="b-main">
		<div id="resp1">
			<div class="b-errors">
				{% for message in form.getMessages() %}
					<span class="errors">{{ message.getMessage() }}</span>
				{% endfor %}
				{% set messages = student.getMessages() %}
				{% if messages !== null %}
					{% for message in student.getMessages() %}
				<span class="errors">{{ message.getMessage() }}</span>
					{% endfor %}
				{% endif %}
			</div>
		<form method="post" action="/students/select" id="form">
			<div id="response" class="error" style="display: none">Ошибка!</div>
			<div class="b-button button-custom button-edit-inf">
				<a href="/students" class="link-back">« Назад</a>
			</div>
			<div>
				<label>Выберите группу:</label>
				<select name="group" id="group"
				        onmousedown="$(':first-child', this).remove(); this.onmousedown = null;">
					<option value=""></option>

					{% for g in group %}
						<option>{{ g.name }}</option>
					{% endfor %}

				</select>
				<input name="year" id="years" type="number" min="1995" max="2050" step="1" value="2017"/>
				<div class="b-button i">
					<input class="button" type="button" id="b1" value="Далее">
				</div>
			</div>
		</form>
		</div>
		<div id="resp2" style="display: none">
			<form method="post" action="/students/add" class="form-edit-inf">
				<div id="resp" style="display: none">
					<input type="hidden" name="group_id" id="group_id">
					<input type="hidden" name="year" id="year">
					{{ formGS.render('status') }}
				</div>
				<label>ФИО:</label>
				{{ form.render('name') }}
				<label>№ студенческого:</label>
				{{ form.render('number') }}
				<label>Логин:</label>
				{{ form.render('login') }}
				<label>Пароль:</label>
				{{ form.render('password') }}
				<input type="hidden" name="role" value=0>
				<input type="hidden" name="status" value=0>
				<div class="b-button button-custom button-edit-inf">
					<a href="/students" class="link-back">« Назад</a>
					<button type="submit" class="button">Добавить</button>
				</div>
			</form>
		</div>
	</div>

</div>
