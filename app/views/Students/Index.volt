<header class="header">
	<div class="b-title-1">
		<span class="text">Электронный дневник</span>
	</div>
	<div class="b-title-2">
		<span class="text">Учащиеся</span>
	</div>
</header>

<nav class="navbar">
	<div class="b-navbar container">
		<ul class="menu-items">
			<li><a href="/info">Основное</a></li>
			<li><a href="/subjects">Предметы</a></li>
			<li><a href="/students" class="active">Учащиеся</a></li>
			<li><a href="/groups">Группы</a></li>
			<li><a href="/move">Переводы</a></li>
		</ul>
		<div class="b-login">
			<a href="/setting" class="login">{{ session.get('login') }}</a>
			<a href="/logout" class="exit">Выход</a>
		</div>
	</div>
</nav>
<div class="main">
	<div class="b-main">
		<form method="post" action="/students/group" id="form2" class="form-progress">
			<div style="display: inline-block" id="resp1">
				<label>Выберите группу:</label>
				<select name="group" id="group"
						onmousedown="$(':first-child', this).remove(); this.onmousedown = null;">
					<option value=0></option>

                    {% for g in group %}
						<option>{{ g.name }}</option>
                    {% endfor %}

				</select>
				<input name="year" id="year" type="number" min="1900" max="2099" step="1" value="2017"/>
				<div class="b-button i">
					<input class="button" type="submit" value="Далее">
				</div>
			</div>
		</form>
	</div>
</div>


